from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi import FastAPI, Request
# from starlette.responses import RedirectResponse
from fastapi.responses import PlainTextResponse
# from fastapi.middleware.cors import CORSMiddleware
from fastapi import Response

import requests

from config_dataclass import ConfigData
from dep_funs import read_file
from models import Item


app = FastAPI(docs_url=None, redoc_url=None)


app.mount(
    "/static",
    StaticFiles(directory=ConfigData.front_path + "static"),
    name="static"
)

templates = Jinja2Templates(directory=ConfigData.front_path + "templates")


@app.get("/")
async def get_root(request: Request):
    secret_header = request.headers.get('X-Custom-Header')
    if secret_header == "4D 41 43 48 49 4E 45":
        print("SUCCESS: ", secret_header)
        return templates.TemplateResponse(
            "cb10b940f5175be688948d8c92cf92a5.html", {"request": request}
        )

    return templates.TemplateResponse(
        "index.html", {"request": request}
    )


@app.post("/")
async def post_root(request: Request, item: Item):
    print("RCE: ", item.url)
    headers = {"X-Custom-Header": "4D 41 43 48 49 4E 45"}
    r = requests.get(item.url, headers=headers)
    return Response(content=r.content)


@app.get("/7f5cb74af5d7f4b82200738fdbdc5a45")
async def manifest(request: Request):
    data = read_file(f"{ConfigData.local_path}manifest.txt")
    return data


@app.get('/robots.txt', response_class=PlainTextResponse)
def robots(request: Request):
    data = read_file(f"{ConfigData.local_path}robots.txt")
    return data
