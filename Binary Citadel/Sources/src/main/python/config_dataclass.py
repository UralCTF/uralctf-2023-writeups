import configparser
from dataclasses import dataclass


@dataclass
class ConfigData:
    front_path = "../resources/src/"
    local_path = "local/"

    config = configparser.ConfigParser()
    config.read("config.ini")
    config_host = str(config["DEFAULT"]["host"])
    config_port = int(config["DEFAULT"]["port"])
