# One little dump - Lost Desktop

**Условие задания:**
> Я успел переименовать и открыть папку, попробуй прочитать её название
[dump.tar.gz](https://disk.yandex.ru/d/fU_HJx3Q1l6jCA)

**Решение:**
Для нахождения профиля операционной системы можно посмотреть решение первой части таска: [One little dump - Saga of Ctrl+c/Ctrl+v](https://gitlab.com/UralCTF/uralctf-2023-writeups/-/tree/main/One%20little%20dump%20-%20Saga%20of%20Ctrl+c-Ctrl+v)

Исходя из описания таска попробуем достать из дампа памяти снимок экрана.

Воспользуемся плагином `screenshot` для получения снимков экрана:
```
volatility.exe -f dump.raw --profile=Win7SP1x64 screenshot -D .\output
```
И получаем вывод команды в виде `.png` файлов.

На одной из полученных картинок с названием `session_1.WinSta0.Default.png` в адресной строке проводника можем [**заметить**](https://gitlab.com/UralCTF/uralctf-2023-writeups/-/tree/main/One%20little%20dump%20-%20Saga%20of%20Ctrl+c-Ctrl+v) строку `J82q7AgdRhFN5HcfQaAP1DuLHXE9m3BTWLeWzg5Qp` в кодировке [Base58](https://ru.wikipedia.org/wiki/Base58).

Декодируем и получаем флаг:
``UralCTF{7h15_dump_15_700_345y}``