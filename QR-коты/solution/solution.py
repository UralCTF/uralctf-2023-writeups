from PIL import Image

task = Image.open("task.png")

size = 33

qr = Image.new("1", (size, size), 255)


pos = [
    (20, 150),
    (200, 210),
    (176, 37),
    (10, 72)
    ]

for i, p in enumerate(pos):
    for x in range(qr.width):
        for y in range(qr.height):
            task_pixel = (p[0]+x, p[1]+y)
            if task.getpixel(task_pixel)[i] != 0:
                qr.putpixel((x,y), 0)

qr = qr.resize((size*10, size*10))

qr.show()

qr.save('solved.png')