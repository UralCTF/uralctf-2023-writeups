# Скопированно из cipher.py
BLOCK_SIZE = 10
LIST_PERMUTATION = [ 4, 9, 6, 1, 0, 5, 7, 8, 2, 3 ]

# Нужно сделать перестановки в обратном порядке (можно и ручками, всего десять элементов)
def reverse_permutation() -> list:
    new_list = [0 for _ in LIST_PERMUTATION]
    for i, p in enumerate(LIST_PERMUTATION):
        new_list[p] = i
    return new_list

REVERSE_PERMUTATION = reverse_permutation()

# Так как блок всего десять символов
# И совпадает с длинной ключа
# Можно убедиться на этой строке
# assert len(key) == BLOCK_SIZE
# то нам нужны толко первые 10 байт из файла

enc_data = open("writeup\social-credit.jpg.enc", "rb").read(BLOCK_SIZE)
data = open("writeup\social-credit.jpg", "rb").read(BLOCK_SIZE)

# Восстанавливаем перестановку

rev_perm_enc_data = b''

for i in REVERSE_PERMUTATION:
    rev_perm_enc_data += enc_data[i].to_bytes()

# Теперь нужно произвести xor чтобы вернуть ключ

key = ""

for a, b in zip(data, rev_perm_enc_data):
    key += chr(a^b)

# Вот и ключ
print(key) # B0Wl0fr1c3


# Нужно восстановить алгоритм дешифровки
# Копируем разбиение на блоки

def divide_blocks(data:bytes) -> bytes:
    for i in range(0, len(data), BLOCK_SIZE):
        yield data[i:i + BLOCK_SIZE]

# Сама функция

def decrypt(file_name:str, key:bytes):
    # Читаем весь файл
    encrypted_data = open(file_name, 'rb').read()

    data = b''

    # Делим на блоки
    for block in divide_blocks(encrypted_data):
        new_block = b''

        # перестановка блока в обратном порядке
        block = b''.join([block[i].to_bytes() for i in REVERSE_PERMUTATION])

        # xor с ключом
        for b, k in zip(block, key):
            new_block += (b^k).to_bytes()

        data += new_block

    # сохраняем наш файл
    decrypted_file = open("decrypted.jpg", "wb")
    decrypted_file.write(data)



decrypt('writeup/flag.jpg.enc', b'B0Wl0fr1c3')