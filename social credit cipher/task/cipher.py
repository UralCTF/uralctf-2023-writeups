BLOCK_SIZE = 10
LIST_PERMUTATION = [ 4, 9, 6, 1, 0, 5, 7, 8, 2, 3 ]


def encrypt(file_name:str, key:bytes):
    data = open(file_name, 'rb').read()
    data = add_padding(data)

    encrypted_data = b''

    for block in divide_blocks(data):
        new_block = b''

        for b, k in zip(block, key):
            new_block += (b^k).to_bytes()

        new_block = b''.join([new_block[i].to_bytes() for i in LIST_PERMUTATION])

        encrypted_data += new_block

    encrypted_file = open(f"{file_name}.enc", "wb")
    encrypted_file.write(encrypted_data)


def decrypt(file_name:str, key:bytes):
    raise NotImplementedError("This function is not available in free version of software")


def add_padding(data:bytes) -> bytes:
    data = data.ljust( (len(data)//BLOCK_SIZE + 1) * BLOCK_SIZE, b'\x00')
    return data


def divide_blocks(data:bytes) -> bytes:
    for i in range(0, len(data), BLOCK_SIZE):
        yield data[i:i + BLOCK_SIZE]


def main():
    key = b''
    user_choise = ""

    print("Please input a file")
    file_name = input(">> ")

    print("[E]ncrypt or [D]ecrypt")
    user_choise = input(">> ")

    print("Please input a password")
    key = bytes(input(">> "), "ascii")


    assert len(key) == BLOCK_SIZE


    if user_choise.lower() == "e":
        encrypt(file_name, key)
    elif user_choise.lower() == "d":
        decrypt(file_name, key)

if __name__ == "__main__":
    main()