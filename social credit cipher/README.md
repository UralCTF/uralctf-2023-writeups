# social credit cipher

**Условие задания:**

>Наши коллеги из Китая скинули нам свою новую версию шифра.
>
>Правда только бесплатную версию, да и ключ приложить забыли.

**Решение:**
Нам дан скрипт на питоне, незашифрованный и зашифрованный файл, а также зашифрованный флаг. Нас ждёт `KPA` (known-plaintext attack), благо у нас есть всё чтобы восстановить флаг.

В файле `cipher.py` нас интересует только пара строк из `main` и одна функция `encrypt`, начнем её разбирать.

В этом таске у нас два задания:

1. Восстановить функцию расшифровки
2. Восстановить ключ

## Начнём с разбора функции encrypt

Считываем весь файл.

```python
    data = open(file_name, 'rb').read()
    data = add_padding(data)
```

Делим на блоки. (Блоки по 10 байт, т.к. `BLOCK_SIZE = 10`)

```python
    for block in divide_blocks(data):
        new_block = b''
```

xor`им блок с ключём.

```python
        for b, k in zip(block, key):
            new_block += (b^k).to_bytes()
```

Делаем перестановку внутри блока.

```python
        new_block = b''.join([new_block[i].to_bytes() for i in LIST_PERMUTATION])
```

и повторяем пока блоки не закончатся.

## Функция расшифровки

Для того чтобы расшифровать информацию нужно воспроизвести обратные действия, сначала перестановка внутри блока затем заксорить.

Перестановка означает что байты просто перемешиваются внутри блока.

Обратить перестановку очень легко, если

> 0 -> 4\
> 1 -> 9\
> 2 -> 6\
> ...\
> 9 -> 3

то обратная функция

> 4 -> 0\
> 9 -> 1\
> 6 -> 2\
> ...\
> 3 -> 9

Построим обратную перестановку

`LIST_PERMUTATION = [ 4, 9, 6, 1, 0, 5, 7, 8, 2, 3 ]` \
`REVERSE_PERMUTATION = [4, 3, 8, 9, 0, 5, 2, 6, 7, 1]`

Далее xor, у этой операции есть важное свойство - `(a ^ b) ^ b == a`. Поэтому нам даже не придётся ничего менять.

Копируем функцию `encrypt`, меняем местами xor и перестановку, не забываем поменять `LIST_PERMUTATION` на `REVERSE_PERMUTATION`.

```python
def decrypt(file_name:str, key:bytes):
    encrypted_data = open(file_name, 'rb').read()

    data = b''

    for block in divide_blocks(encrypted_data):
        new_block = b''

        block = b''.join([block[i].to_bytes() for i in REVERSE_PERMUTATION])

        for b, k in zip(block, key):
            new_block += (b^k).to_bytes()

        data += new_block

    decrypted_file = open("decrypted.jpg", "wb")
    decrypted_file.write(data)
```

## Восстановление ключа

Все готово, осталось получить ключ. Вспомним про свойство xor еще раз.

`c = m^k --> k = c^m`

Все что нам нужно чтобы получить ключ это заксорить шифртекст с открытым текстом, не забывая сделать перестановку.

Длинну ключа мы знаем из этой строчки из `main`

`assert len(key) == BLOCK_SIZE`, т.е. len(key) == 10

Читаем по 10 байт из `social-credit.jpg` и `social-credit.jpg.enc`

```python
enc_data = open("writeup\social-credit.jpg.enc", "rb").read(BLOCK_SIZE)
data = open("writeup\social-credit.jpg", "rb").read(BLOCK_SIZE)
```

Делаем перестановку

```python
rev_perm_enc_data = b''

for i in REVERSE_PERMUTATION:
    rev_perm_enc_data += enc_data[i].to_bytes()
```

И ксорим

```python
key = ""

for a, b in zip(data, rev_perm_enc_data):
    key += chr(a^b)
```

Получаем наш ключ `key == b'B0Wl0fr1c3'`.

Дешифруем и получаем флаг - `UralCTF{Plus_Inf1n1t3_S0c14l_Cr3d14s}`.

Полный скрипт со всем что мы сделали - `solution\solution.py`
