from fastapi import FastAPI, Request, Response

from config_dataclass import ConfigData
from models import Key
import functions


app = FastAPI(docs_url=None, redoc_url=None)


@app.post("/license")
async def post_license_key(request: Request, item: Key):
    res = functions.verify_pub_key(item.key, ConfigData.config_license)
    print(item.key)
    print(res)
    return res


@app.post("/file")
async def post_key(request: Request, item: Key):
    res = functions.verify_pub_key(item.key, ConfigData.config_key)
    print(item.key)
    print(res)
    if res:
        return Response(ConfigData.flag, status_code=200)
    else:
        return Response("Invalid public key", status_code=403)
