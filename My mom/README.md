# Mymom?
**Условие задания:**
Моя мама очень любит использовать эмоджи в своих сообщениях, но, кажется, в последнее время она стала совсем немного перебарщивать с этим... Как мне понять, что она хотела сказать? Формат флага : UralCTF{word_word_word}, слова вписать транскрипцией

**Решение:**
Нужно провести частотный анализ текста. Считаем количество вхождений каждого уникального смайлика, после чего получаем определенное соотношение. Например: смайлик 😀 встречается в тексте чаще всего, причем со значительным отрывом от других.  После него идет 😃, затем 😄, а затем 😁. Если посмотреть их процентное соотношение по распределению в тексте, то можно заметить, что оно полностью совпадает с частотным соотношением букв в русском алфавите. Соответственно можно предположить: 😀- о, 😃- е и т.д 
Составив полную таблицу замены получим практическую точную расшифровку текста, но есть небольшая проблема: буквы М,Д,П,У в русском языке встречаются с очень схожей частотой, которую можно различить только на огромных массивах данных. Но перебрать все значения не составит труда, т.к у нас всего одна неоднозначная позиция в флаге, имеющая всего четыре возможных значения. 
флаг: сонимрак 
flag: `UralCTF{son_i_mrak}`
