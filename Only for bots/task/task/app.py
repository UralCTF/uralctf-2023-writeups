from typing import Annotated

from fastapi import FastAPI, Request, Header, HTTPException
from fastapi.responses import JSONResponse, PlainTextResponse
from fastapi.exception_handlers import http_exception_handler

from packaging.version import parse as parse_version, Version, InvalidVersion
from user_agents import parse as parse_ua

app = FastAPI(docs_url=None, redoc_url=None)

status = {
    "flags_requested": 0,
    "flags_given": 0,
    "app_errors": 0,
}


@app.get("/")
def index():
    return [{"message": "Hello, human!"}, {"message": "RECONFIGURE_YOURSELF_IF_ROBOT"}]


@app.get("/health")
def health():
    return {
        "healthy": True,
        "status": status,
    }


def get_ua_and_version(ua_string: str) -> tuple[str, tuple[int, ...]]:
    result = parse_ua(ua_string)
    return result.browser.family, result.browser.version


@app.get("/flag")
def flag(user_agent: Annotated[str | None, Header()] = None):
    status["flags_requested"] += 1
    if user_agent is None:
        raise HTTPException(
            status_code=400,
            detail={"error": "CANNOT_DETERMINE_CLIENT"},
        )
    browser, version = get_ua_and_version(user_agent)
    if browser != "FlagBot":
        raise HTTPException(
            status_code=403,
            detail={
                "error": "INVALID_CLIENT",
                "expected": "FlagBot",
                "actual": browser,
            },
        )
    try:
        parsed_version = parse_version(".".join(str(v) for v in version))
    except InvalidVersion:
        raise HTTPException(
            status_code=403,
            detail={"error": "CANNOT_DETERMINE_CLIENT_VERSION"},
        )
    if parsed_version < parse_version("1.33.7"):
        raise HTTPException(
            status_code=403,
            detail={
                "error": "INVALID_VERSION",
                "expected": ">=1.33.7",
                "actual": str(parsed_version),
            },
        )
    status["flags_given"] += 1
    return {
        "flag": "UralCTF{User-Agent_is_not_a_security_measure}",
        "browser": browser,
        "version": str(parsed_version),
    }


@app.get("/robots.txt")
def robots():
    return PlainTextResponse(
        content="User-Agent: *\nAllow: /flag\nAllow: /robots.txt\nDisallow: /",
    )


@app.exception_handler(Exception)
async def handle_exception(request: Request, exc: Exception):
    status["app_errors"] += 1
    if isinstance(exc, HTTPException):
        return await http_exception_handler(request, exc)
    return JSONResponse(
        status_code=500,
        content={
            "detail": {
                "error": "INTERNAL_SERVER_ERROR",
                "type": type(exc).__name__,
                "message": str(exc),
            },
        },
    )
