# One little dump - In the name of Dora...

**Условие задания:**
> Кажется, я запустил какой-то вирус...
[dump.tar.gz](https://disk.yandex.ru/d/fU_HJx3Q1l6jCA)

**Решение:**
Для нахождения профиля операционной системы можно посмотреть решение первой части таска: [One little dump - Saga of Ctrl+c/Ctrl+v](https://gitlab.com/UralCTF/uralctf-2023-writeups/-/tree/main/One%20little%20dump%20-%20Saga%20of%20Ctrl+c-Ctrl+v)

Исходя из описания таска попробуем посмотреть, какие процессы были запущены. 
Воспользуемся плагином `pstree`:
```
volatility.exe -f dump.raw --profile=Win7SP1x64 pstree
```
И получаем вывод команды, где можем заметить подозрительный процесс `EvilCrook.exe`, который запускает ненастоящий процесс `Explorer.exe`.
```
Name                                                  Pid   PPid   Thds   Hnds Time
 0xfffffa8003132b30:explorer.exe                     1584   1436     36    895 2023-10-02 18:28:16 UTC+0000
. 0xfffffa8000e6c060:EvilCrook.exe                   1972   1584      6     78 2023-10-02 18:28:52 UTC+0000
.. 0xfffffa8003361630:Explorer.exe                    352   1972      2     49 2023-10-02 18:28:52 UTC+0000
```

Попробуем посмотреть, с какими параметрами был запущен процесс `Explorer.exe` с PID'ом `352`. Для этого воспользуемся плагином `cmdlne`:
```
volatility.exe -f dump.raw --profile=Win7SP1x64 cmdline -p 352
```
И получаем вывод команды, где [**опять**](https://gitlab.com/UralCTF/uralctf-2023-writeups/-/tree/main/One%20little%20dump%20-%20Saga%20of%20Ctrl+c-Ctrl+v) видим строку в кодировке [Base58](https://ru.wikipedia.org/wiki/Base58).

>Explorer.exe pid:    352
Command line : "Explorer.exe" -r --force **9XW3NVxB4StDRsqnFps6BJNuebXAHJJfvcNra7KH4JewqZnC**

Декодируем и получаем флаг:
``UralCTF{y0u_4r3_7ru3_f0r3n51c_6uru}``