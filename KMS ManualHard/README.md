# KMS ManualHard

**Условие задания:**

>На просторах интернета я нашел забытую версию Windows, но так и не смог её установить.
>
>Сможешь восстановить ключ, который она требует?

**Решение:**

Первое что можно заметь что файл `.msc` не определяется как запускаемый, всё верно ведь в названии `Windows9X.msс` буква `с` - русская.

Теперь нам нужно определить что за файл лежит перед нами.

Воспользуемся утилитой `file` в линуксе.

```bash
$ file Windows9X.msс
Windows9X.msс: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=fa14e7c94b49d9892133a07b17dcf41eb8ddc8a0, for GNU/Linux 3.2.0, not stripped
```

`ELF 64-bit for GNU/Linux` - то что мы и хотели узнать. [ELF — это сокращение от Executable and Linkable Format, как правило, является выходным файлом компилятора](https://habr.com/ru/articles/480642/). Отлично и что с этим делать.

Я буду пользоваться [IDA Free](https://hex-rays.com/ida-free/#download) из-за его удобства, несмотря и ограниченный функционал (P.S. мы все знаем где можно приобритать игры и программы со скидкой 100%).

![IDA view-A](solution/ida_view-a.png)

Выглядит страшно и непонятно, благо по одному нажатию `F5` мы видим самый обычный код. Для удобства переименовал переменную `key`. Дальше переменные будут тоже переименованы, просто учтитие что вы будете видеть у себя `v1`, `v2`, ... , `vn`.

```c
int __cdecl main(int argc, const char **argv, const char **envp)
{
  __int64 key; // [rsp+8h] [rbp-8h] BYREF

  printf(
    "Welcome to Windows 9X activation program.\n"
    "Thank you for choosing MacroHard product.\n"
    "One step to go, please input your 11 digit CD-Key...\n"
    ">> ");
  __isoc99_scanf("%ld", &key);
  if ( !(unsigned int)check_length(key) )
    return -1;
  puts("First check complete.");
  if ( !(unsigned int)check_divisibility(key) )
    return -1;
  puts("Second check complete.");
  if ( !(unsigned int)check_forbidden(key) )
    return -1;
  puts("Congrats!");
  print_flag(key);
  return 0;
}
```

Считываем ключ и делаем три проверки, должно быть просто. Перейдем в функцию `check_length`.

```c
_BOOL8 __fastcall check_length(__int64 key)
{
  int length; // [rsp+14h] [rbp-4h]

  length = 0;
  while ( key )
  {
    key /= 10LL;
    ++length;
  }
  return length == 11;
}
```

Быстренько компилируем в уме и понимаем что длина ключа должа быть равна `11`. Идём дальше, `check_divisibility`.

```c
_BOOL8 __fastcall check_divisibility(__int64 key)
{
  int sum; // [rsp+14h] [rbp-4h]

  sum = 0;
  while ( key )
  {
    sum += (int)key % 10;
    key /= 10LL;
  }
  return sum % 7 == 0;
}
```

Снова все обмозговали и поняли что сумма цифр должна делиться на `7`. Дальше `check_forbidden`.

```c
__int64 __fastcall check_forbidden(__int64 key)
{
  int num; // [rsp+10h] [rbp-8h]
  int i; // [rsp+14h] [rbp-4h]

  num = key / 100000000;
  if ( num == 123 )
    return 0LL;
  for ( i = 1; i <= 9; ++i )
  {
    if ( num == 111 * i || num == 100 * i )
      return 0LL;
  }
  return 1LL;
}
```

Как и до этого думаем и понимаем что ключ не должен начинаться с `123`, `111` ... `999`, `100` ... `900`.

Итого:

* длина ключа должа быть равна `11`
* сумма цифр должна делиться на `7`
* ключ не должен начинаться с `123`, `111` ... `999`, `100` ... `900`

Придумываем такой ключ, например - `10100000005`. (P.S. не забываем `chmod +x Windows9X.msс` чтобы сделать файл исполняемым)

```text
$ ./Windows9X.msс
Welcome to Windows 9X activation program.
Thank you for choosing MacroHard product.
One step to go, please input your 11 digit CD-Key...
>> 10100000005
First check complete.
Second check complete.
Congrats!
UralCTF{r3v3r5e_i5_e4s3r_th4n_y0u_7h0ught}
```

Флаг - `UralCTF{r3v3r5e_i5_e4s3r_th4n_y0u_7h0ught}`

---

Мы так и не рассмотрели функцию `print_flag`.

```c
int __fastcall print_flag(__int64 a1)
{
  int v3[45]; // [rsp+10h] [rbp-C0h]
  int v4; // [rsp+C4h] [rbp-Ch]
  int i; // [rsp+C8h] [rbp-8h]
  int v6; // [rsp+CCh] [rbp-4h]

  v6 = 0;
  while ( a1 )
  {
    v6 += (int)a1 % 10;
    a1 /= 10LL;
  }
  v4 = 42;
  v3[0] = 43;
  v3[1] = 23;
  v3[2] = 37;
  v3[3] = 31;
  v3[4] = 65;
  v3[5] = 49;
  v3[6] = 60;
  v3[7] = 10;
  v3[8] = 30;
  v3[9] = 92;
  v3[10] = 0;
  v3[11] = 94;
  v3[12] = 26;
  v3[13] = 70;
  v3[14] = 21;
  v3[15] = 46;
  v3[16] = 15;
  v3[17] = 90;
  v3[18] = 51;
  v3[19] = 8;
  v3[20] = 64;
  v3[21] = 0;
  v3[22] = 67;
  v3[23] = 1;
  v3[24] = 57;
  v3[25] = 13;
  v3[26] = 2;
  v3[27] = 79;
  v3[28] = 14;
  v3[29] = 60;
  v3[30] = 17;
  v3[31] = 79;
  v3[32] = 43;
  v3[33] = 0;
  v3[34] = 107;
  v3[35] = 59;
  v3[36] = 116;
  v3[37] = 46;
  v3[38] = 63;
  v3[39] = 63;
  v3[40] = 60;
  v3[41] = 42;
  for ( i = 0; i < v4; ++i )
    putchar(((v6 % 7) ^ i ^ v3[i] ^ 0x7B) + 5);
  return putchar(10);
}
```

Если смотреть вдумчиво, до можно понять что единственное что изменяется в этой функции от ключа это `(v6 % 7)` в `putchar(((v6 % 7) ^ i ^ v3[i] ^ 0x7B) + 5);`.

`(v6 % 7)` даёт нам всего 6 возможных вариантов. Слегка изменяем функцию и запускаем `print_flag(0)`

```c
#include <stdio.h>

int print_flag(long a1)
{
  int v3[45]; // [rsp+10h] [rbp-C0h]
  int v4; // [rsp+C4h] [rbp-Ch]
  int i; // [rsp+C8h] [rbp-8h]
  int v6; // [rsp+CCh] [rbp-4h]

  v6 = 0;
  while ( a1 )
  {
    v6 += (int)a1 % 10;
    a1 /= 10LL;
  } 
  v4 = 42; 
  v3[0] = 43; v3[1] = 23;
  v3[2] = 37; v3[3] = 31; v3[4] = 65; v3[5] = 49;
  v3[6] = 60; v3[7] = 10; v3[8] = 30; v3[9] = 92;
  v3[10] = 0; v3[11] = 94; v3[12] = 26; v3[13] = 70;
  v3[14] = 21; v3[15] = 46; v3[16] = 15; v3[17] = 90;
  v3[18] = 51; v3[19] = 8; v3[20] = 64; v3[21] = 0;
  v3[22] = 67; v3[23] = 1; v3[24] = 57; v3[25] = 13;
  v3[26] = 2; v3[27] = 79; v3[28] = 14; v3[29] = 60;
  v3[30] = 17; v3[31] = 79; v3[32] = 43; v3[33] = 0;
  v3[34] = 107; v3[35] = 59; v3[36] = 116; v3[37] = 46;
  v3[38] = 63; v3[39] = 63; v3[40] = 60; v3[41] = 42;

  for ( i = 0; i < v4; ++i )
    putchar(((v6 % 7) ^ i ^ v3[i] ^ 0x7B) + 5);
  return putchar(10);
}

int main()
{
    print_flag(0);

    return 0;
}
```

И получаем тот же самый флаг - `UralCTF{r3v3r5e_i5_e4s3r_th4n_y0u_7h0ught}`