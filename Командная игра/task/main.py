import asyncio
import emoji as e
import aiosqlite
import telebot
import aiohttp
from telebot import types
from telebot.asyncio_storage import StateMemoryStorage
from telebot.asyncio_handler_backends import State, StatesGroup
from telebot import TeleBot
from telebot import asyncio_filters
from telebot.async_telebot import AsyncTeleBot

Bot_key = "6400256357:AAEfKwArtzeUq4HhBZbxHht6j0IQeqlSfYk"
Chat_to_send=-4059646445
flag="UralCTF{MinecraftIsMyLifeFRNMGOlk1qGMzZuO}"

bot = AsyncTeleBot(Bot_key, state_storage=StateMemoryStorage())

class MyStates(StatesGroup):
    inputcomandname = State()
    inputyesornot = State()


@bot.message_handler(commands=['start'])
async def start(m: types.Message):
    await bot.send_message(m.from_user.id, "Просто отправьте фото с вашей командой мне и я отправлю его на модерацию!\n\nНезабывайте что на фото должно быть как минимум 2 и на фоне должна быть надпись UralCTF!")


@bot.message_handler(content_types=["photo"])
async def Get_photo(m):
    if m.chat.type != "private":
        return
    db = await aiosqlite.connect('data.sqlite')
    c = await db.execute("select * from users where userid=? and adminchecked=0",(str(m.from_user.id),))
    user=await c.fetchone()
    if user!=None:
        await db.close()
        await bot.send_message(m.from_user.id,"Дождитесь модерации прошлого запроса!")
        return
    c = await db.execute("select * from users where userid=? and banned=1", (str(m.from_user.id),))
    user = await c.fetchone()
    if user!=None:
        await db.close()
        await bot.send_message(m.from_user.id,"Вы забанены!")
        return
    await db.close()

    #file_info = await bot.get_file(m.photo[len(m.photo) - 1].file_id)

    #await bot.send_photo(m.from_user.id,photo=m.photo[len(m.photo) - 1].file_id)

    await bot.set_state(m.from_user.id, MyStates.inputcomandname)
    async with bot.retrieve_data(m.from_user.id) as data:
        data['file_id']=m.photo[len(m.photo) - 1].file_id

    await bot.send_message(m.from_user.id,"Введите название своей команды!")

@bot.message_handler(state= MyStates.inputcomandname,content_types=["text"])
async def Get_comandname(m):
    await bot.set_state(m.from_user.id, MyStates.inputyesornot)
    async with bot.retrieve_data(m.from_user.id) as data:
        data['comand_name'] = m.text
        file_id=data['file_id']

    Butt_yes_or_not = types.ReplyKeyboardMarkup(resize_keyboard=True)
    Butt_yes_or_not.add(types.KeyboardButton("Да"),types.KeyboardButton("Нет"))

    await bot.send_photo(m.from_user.id,photo=file_id,caption=f"Давайте проверим все верно?\n\nКоманда: {str(m.text)}",reply_markup=Butt_yes_or_not)

@bot.message_handler(state= MyStates.inputyesornot,content_types=["text"])
async def Get_yes_or_not(m):
    if m.text=="Нет":
        await bot.delete_state(m.from_user.id)
        await bot.reset_data(m.from_user.id)

        await bot.send_message(m.from_user.id,"Хорошо, попробуйте заново!",reply_markup=types.ReplyKeyboardRemove())
    elif m.text=="Да":

        async with bot.retrieve_data(m.from_user.id) as data:
            comand_name = data['comand_name']
            file_id = data['file_id']
        await bot.delete_state(m.from_user.id)
        await bot.reset_data(m.from_user.id)
        try:
            username = "@" + str(m.from_user.username)
        except:
            username = str(m.from_user.id)

        keybooard_for_admins = types.InlineKeyboardMarkup()
        keybooard_for_admins.add(
            types.InlineKeyboardButton(e.emojize("Забанить"),
                                       callback_data=f"BAN:{str(m.from_user.id)}"),
            types.InlineKeyboardButton(e.emojize(":check_mark_button: Принять"),
                                       callback_data=f"YES:{str(m.from_user.id)}")
            )
        keybooard_for_admins.add(types.InlineKeyboardButton(e.emojize(":no_entry:Отказать"),
                                       callback_data=f"NO:{str(m.from_user.id)}"))

        db = await aiosqlite.connect('data.sqlite')
        await db.execute("insert into users (userid,username,team_name) values (?,?,?)", (str(m.from_user.id),str(username),str(comand_name)))
        await db.commit()

        await bot.send_photo(Chat_to_send,photo=file_id,caption=f"Пользователь: {username} ({str(m.from_user.id)})\n\nКоманда: {comand_name}",reply_markup=keybooard_for_admins)
        await bot.send_message(m.from_user.id,"Фото отправлено на модерацию, ожидайте!",reply_markup=types.ReplyKeyboardRemove())


@bot.callback_query_handler(func=lambda c: 'YES:' in c.data)
async def Yes_user(call: types.CallbackQuery):
    userid=str(call.data).split(":")[1]

    db = await aiosqlite.connect('data.sqlite')
    await db.execute("update users set banned=1, approved=1, adminchecked=1 where userid=?",
                     (str(userid),))
    await db.commit()

    await bot.send_message(userid,f"Поздравляю! Вы прошли модерацию!\n\nВаш ключ: <code>{str(flag)}</code>",parse_mode="HTML",reply_markup=types.ReplyKeyboardRemove())
    await bot.edit_message_reply_markup(call.message.chat.id,call.message.id,reply_markup=None)

@bot.callback_query_handler(func=lambda c: 'NO:' in c.data)
async def Yes_user(call: types.CallbackQuery):
    userid=str(call.data).split(":")[1]

    db = await aiosqlite.connect('data.sqlite')
    await db.execute("update users set banned=0, approved=0, adminchecked=1 where userid=?",
                     (str(userid),))
    await db.commit()

    await bot.send_message(userid,f"Вы не прошли модерацию, попробуйте заново!",parse_mode="HTML",reply_markup=types.ReplyKeyboardRemove())
    await bot.edit_message_reply_markup(call.message.chat.id,call.message.id,reply_markup=None)

@bot.callback_query_handler(func=lambda c: 'BAN:' in c.data)
async def Yes_user(call: types.CallbackQuery):
    userid=str(call.data).split(":")[1]

    db = await aiosqlite.connect('data.sqlite')
    await db.execute("update users set banned=1, approved=0, adminchecked=1 where userid=?",
                     (str(userid),))
    await db.commit()

    await bot.send_message(userid,f"Вы забанены!",parse_mode="HTML",reply_markup=types.ReplyKeyboardRemove())
    await bot.edit_message_reply_markup(call.message.chat.id,call.message.id,reply_markup=None)


bot.add_custom_filter(asyncio_filters.StateFilter(bot))
if __name__ == '__main__':
    asyncio.run(bot.polling(non_stop=True, interval=0, request_timeout=60, timeout=60))